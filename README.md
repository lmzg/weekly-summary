# weekly-summary
技术每周总结，hexo博客

## 初次安装

### clone

```bash
git clone git@gitee.com:lmzg/weekly-summary.git
```

### 安装hexo 

```bash
cnpm i -g hexo
```

### 初始化

```bash
cnpm i
```

## 数据同步

如果不是第一次使用，需要先和线上的数据同步

```bash
git pull
```

## 开始更新

### 新建博客

```bash
hexo n <文件名>
```

### 部署博客

```bash
hexo g -d
```

