---
title: 20200716-Markdown与Hexo
date: 2020-07-17 14:17:57
description: 制定培训计划和制度， Markdown的使用，hexo的安装和使用
tags:
  - 工具
  - Markdown
  - Hexo
---

|||
|:---:|:---:|
|会议时间：|2019-04-17|
|记录人：|<qq>jnn</qq>|
|参会人员：| <qq>wrj</qq> <qq>zk</qq> <qq>wjj</qq> <qq>lzr</qq> <qq>jnn</qq> <qq>yll</qq> <qq>dzy</qq>
|请假人员：|<qq>lss</qq>|

## 制定培训计划和制度
|||
|:---:|:---:|
|培训时间|7月至元旦前，大概两周一次|
|培训制度|俊俊负责拍照记录，每周的负责人做好会议记录，并且需要上传|
|会议记录人员顺序| <qq>jnn</qq> <qq>zk</qq> <qq>lzr</qq> <qq>wjj</qq> <qq>yll</qq> <qq>dzy</qq> <qq>lss</qq>

## Markdown的使用和重点标签的讲解

### 使用

1. 工具 `VS Code `
2. 插件: `Markdown All in One`

### 标签讲解 

1. 标题标签 `#`
2. 无序和有序列表 `-`
3. 加粗`**`、p标签、斜体`*`、分割线`***`、删除线`~~`
4. 表制作
5. 引用内容`>`
6. 链接`[...](...)`
7. 生成当前页面目录`[TOC]`
8. 简单的数学公式表达`$$`
9. 文本原样输出

## hexo的安装和使用

安装的注意事项：电脑依赖nodejs环境，利用npm或者cnpm下载，一定要加入到环境变量

[使用hexo在github上搭建免费博客](https://www.jeay.net/tools/hexo-blog.html)
