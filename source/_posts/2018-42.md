---
title: 2018-42
date: 2018-10-18 09:04:22
tags:
description: 18年第42周，品牌技术交流会会议记录
---

# 2018年第42周技术周总结

|会议记录|详细|
|:---:|:---:|
|会议时间：|2018-10-18|
|所属组：|UED技术组|
|主持人：|<qq>wrj</qq>|
|参会人员：|<qq>lzr</qq> <qq>yxc</qq> <qq>wjj</qq> <qq>lss</qq> <qq>hs</qq>  <qq>zk</qq> <qq>wrj</qq> |
|缺席人员：|<qq>jys</qq> <qq>gzl</qq>|

---

## 本周工作总结

<qq>lzr</qq>

- 上海站108个英文模板调整js位置
- 优化模板20%
- 1个视频添加字幕
- 上海站四语种证书排查完成

<qq>yxc</qq>

- lq、zg 46个 关键词排名查询记录
- lq 产品内页重构完成，产品调整完成19个
- lmzg证书排查完成

<qq>wjj</qq>

- breakday企业新闻证书排查
- breakday新案例栏目上线
- kefidchina路由测试

<qq>lss</qq>

- 模型管理功能完成
- 提示内容和样式修改
- breakday行业新闻证书排查

<qq>hs</qq>

- 信息管理系统
  - 信息列表、信息详情
  - 用户列表
- 阿语站一篇新文章上线

<qq>zk</qq>

- 优化模板45%

<qq>wrj</qq>

- 信息处理系统
  - 完成信息标记、信息备注功能及相关websocket配置和前端的通信测试
  - 重做websocke的连接验证，使用房间区分用户权限，控制广播范围
  - 新增通过第三方API获取IP地址信息功能，增加ip信息redis缓存，减少外部api请求次数
  - 引用阿里短信sdk，接入短信接口，增加新用户手机号短信验证、新地址登陆短信验证
- 工作管理系统新增个人总结按时间排序以及bug修正：修正因时差导致部分工作总结不显示的问题

<qq>jys</qq>

- 请假

<qq>gzl</qq>

- 请假

---

## 本月冲刺目标完成情况

<qq>lzr</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
优化模板|3套|第一套完成20%|7%|无法完成
视频|制作8个，上线10个|4个完成，2制作中，2待翻译|60%|可以完成

<qq>yxc</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
产品内页|重构|完成，需要结合各产品内页进行调试|99%|完成
产品内页|上线43个产品|完成19个|44%|可以完成

<qq>wjj</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
kefidchina|细节调整完成|设计师没有新的意见提出|90%|可以完成
break-day|细节调整完成|案例栏目完成，剩余关于等单页待推进|60%|可以完成

<qq>lss</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
网站后台|达到内测标准|主要功能完成，进行细节优化及美化|97%|可以完成

<qq>hs</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
信息处理系统|完成所有前端页面|核心框架完成、消息、用户栏目完成|60%|可以完成

<qq>zk</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
证书排查|空|孟召自己完成了？|无|完成
优化模板|3套|一套完成45%|15%|可以达到80%

<qq>wrj</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
信息处理系统|完成所有api工作|完成|100%|完成
信息处理系统|完成websocket模块|主要功能调试成功|90%|可以完成

<qq>gzl</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
新案例|模板重构|基本完成|90%|可以完成
新案例|上线案例20|上线3个|15%|可以完成

<qq>jys</qq>

任务|目标|当前进度|总体进度占比|自我评估
-|-|-|-|-
证书排查|-|已完成|100%|已完成
优化模板|3套|完成一套|33%|可以完成

---

## 分享

<qq>zk</qq>

[百度地图API简单介绍](https://www.zybuluo.com/zke/note/1312360)
