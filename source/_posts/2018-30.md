---
title: 2018-30
date: 2018-08-02 08:10:53
tags:
description: 18年第30周，品牌技术交流会会议记录
---

# 2018年第29周技术周总结

|会议记录|详细|
|:---:|:---:|
|会议时间：|2018-07-25|
|所属组：|UED技术组|
|主持人：|<qq>lzr</qq>|
|参会人员：|<qq>wjj</qq> <qq>wrj</qq> <qq>jys</qq> <qq>hs</qq> <qq>lss</qq>  <qq>yxc</qq> <qq>zk</qq> <qq>lzr</qq>|

## 本周工作总结

<qq>lzr</qq>

- lmzggroup更换图片两张
- lmzggroup添加英文案例两个
- lmzggroup更换惊奇产品和砂石骨料专题的框架



<qq>zk</qq>

- lq老专题升级重构测试修改完成
- 优化模板中文破碎站重构完成15%


<qq>jys</qq>

- kefidchina矿渣专题重构完成，细节修改完成
- kefidchina法语站新闻内页样式调整
- kefidchina矿山破碎这周重构完成

<qq>yxc</qq>

- lmzg首页banner换图1张,应用领域换图3张,电厂脱硫专题换图3张
- 违禁词排查页面170个


<qq>lss</qq>

- lmlq河卵石专题升级完成，上线
- kefidchina阿语站商务通代码修改，https排查
- kefidchina西语法语站头部邮箱错误修改


<qq>wrj</qq>

- 248服务器自动备份功能上线，完成lmlq服务器所有内容同步
- 技术通道知识点、试题完成
- 新服务器配置kefidchina站点


<qq>hs</qq>

- kefid及kefidchina各语种新服务器数据库配置和dede版本升级（14个站）
- kefid中文专题，模板和样式分离（7个）
- 中文智能石灰石专题上线

<qq>gzl</qq>

- lmlq违禁词排查修改96+处


<qq>wjj</qq>

- break-day首页细节全部调整


****


# 技术分享

<qq>lzr</qq>

 [关于js中this的用法详解][3]

- 函数调用方式(输出为`window`)
- 对象调用方式(输出为对象的name属性值)
- 构造函数模式(输出为函数中的name的值)
- apply调用模式(输出为继承的对象的name值,被覆盖则输出覆盖后的值)  


<qq>yxc</qq>

php数组遍历的四种方式:

- for循环遍历
- 使用list()/each()/while()配合遍历
- foreach循环遍历
- 指针遍历

<qq>lss</qq>

框架图片和字体排布关系:

- 拼图框架有:`x*-left/x*-right`和`x*-move`两种形式
- bootstrap框架有`.col-md-offset-*`和`.col-md-push-*/.col-md-pull-*`两种形式  

<qq>wjj</qq>

用js书写一个99乘法表,利用遍历的思想完成  

```javascript   
var str='';    
for (i=1; i<=9;i++) {    
    for(j=i;j<=9;j++) {    
        str +=i+'*'+j+'='+j*i+' ';    
        }    
    }    
    str+='\n';        
```


<qq>wrj<qq>

1. [基于php的api转发及原生js的前端][1]

2. [js的函数和对象的定义方式][2]


[1]: http://192.168.7.196:8088/index.php?share/file&user=207&sid=f6PmIaek 
[2]: http://192.168.7.196:8088/index.php?share/file&user=207&sid=x3JuEYRN  
[3]: http://192.168.7.196:8088/index.php?share/file&user=209&sid=3wnsT5ir