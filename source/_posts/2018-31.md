---
title: 2018-31
date: 2018-08-08 17:20:33
tags:
description: 18年第31周，品牌技术交流会会议记录
---

# 2018年第29周技术周总结

|会议记录|详细|
|:---:|:---:|
|会议时间：|2018-08-08|
|所属组：|UED技术组|
|主持人：|<qq>zk</qq>|
|参会人员：|<qq>wjj</qq> <qq>wrj</qq> <qq>jys</qq> <qq>hs</qq> <qq>lss</qq>  <qq>yxc</qq> <qq>gzl</qq> <qq>zk</qq> <qq>lzr</qq>|

## 本周工作总结

<qq>lzr</qq>

- lmzggroup案例添加1个
- lmzggroup非金属矿框架替换2个
- 视频字幕整理1个


<qq>zk</qq>

- 优化模板中文破碎站重构完成100%


<qq>jys</qq>

- kefid中文违禁图替换31个
- kefid案例添加1个
- 违禁词修改112个
- kefidchina中英文显示问题修改

<qq>yxc</qq>

- zggroup违禁词页面排查69个
- lmlq10个物料链接细节修改
- lmlq28个违禁图替换


<qq>lss</qq>

- tp5新闻列表\详细页内容展示


<qq>wrj</qq>

- vnpt kefid页面静态化
- kefid留言php7.2兼容性调整
- 信息管理系统头部样式、用户信息、登录请求


<qq>hs</qq>

- kefid和kefidchina各语种网站转移服务器完成
- 排查kefid及kefidchina网站着陆页是否访问正常
- 解决kefid及kefidchina转移网站遗留问题（6处）
- kefid上线英文案例3个

<qq>gzl</qq>

- 31页文档违禁词排查
- 14张版权图片替换


<qq>wjj</qq>

- break-day产品内页重构并上线
- 48个产品图替换
- 简介内容特点修改


****


# 技术分享

<qq>wjj</qq>

用js进行鸡兔同笼计算

- 通过抬腿法进行分析计算
- 通过循环遍历进行计算

<qq>jys</qq>

css cursor 鼠标样式列表整理


<qq>lzr</qq>

[BeautifulSoup的使用方法][1]

<qq>yxc</qq>

[利用php正则表达式抓取页面内容存入数据库][2]

<qq>zk</qq>

[ES6 类（class） 以及继承方法][3]


<qq>wrj<qq>

py面向对象的实例应用

[1]: http://www.lzrtop.com/2018/08/07/BeartifulSoup%E7%9A%84%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95/
[2]: http://www.phpor.top/2018/07/27/%E5%88%A9%E7%94%A8php%E6%AD%A3%E5%88%99%E8%A1%A8%E8%BE%BE%E5%BC%8F%E6%8A%93%E5%8F%96%E9%A1%B5%E9%9D%A2%E5%86%85%E5%AE%B9%E5%AD%98%E5%85%A5%E6%95%B0%E6%8D%AE%E5%BA%93/
[3]: https://www.zybuluo.com/zke/note/1242378